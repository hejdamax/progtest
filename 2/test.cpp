#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <memory>
#include <functional>
#include <stdexcept>
#include <algorithm>
using namespace std;
#endif /* __PROGTEST__ */

class CPerson
{
public:
  CPerson(const string &name, const string &addr, const string &account);
  string getName() { return this -> name; };
  string getAddress() { return this -> address; };
  string getAccount() { return this -> account; };
  int getIncome() { return this -> income; }
  int getExpense() { return this -> expense; }
  void addIncome(int amount);
  void addExpense(int amount);
  void setAccount(const string &account);
private:
  string name;
  string address;
  string account;
  int income;
  int expense;
};

class CIterator
{
public:
  bool AtEnd(void) const;
  void Next(void);
  string Name(void) const;
  string Addr(void) const;
  string Account(void) const;
  void setIterator(vector<shared_ptr<CPerson>>::const_iterator, vector<shared_ptr<CPerson>>::const_iterator);
private:
  vector<shared_ptr<CPerson>>::const_iterator dbIterator;
  vector<shared_ptr<CPerson>>::const_iterator dbEnd;
};

class CTaxRegister
{
public:
  bool Birth(const string &name, const string &addr, const string &account);
  bool Death(const string &name, const string &addr);

  bool Income(const string &account, int amount);
  bool Income(const string &name, const string &addr, int amount);

  bool Expense(const string &account, int amount);
  bool Expense(const string &name, const string &addr, int amount);

  bool Audit(const string &name, const string &addr, string &account, int &sumIncome, int &sumExpense) const;

  CIterator ListByName(void) const;

private:
  vector<shared_ptr<CPerson>> database;
  vector<shared_ptr<CPerson>> index;
};

bool cmpNA(shared_ptr<CPerson> lhs, shared_ptr<CPerson> rhs)
{
  if (lhs->getName().compare(rhs->getName()) < 0)
  {
    return true;
  }
  else if (lhs->getName().compare(rhs->getName()) == 0)
  {
    if (lhs->getAddress().compare(rhs->getAddress()) < 0)
    {
      return true;
    }
  }
  return false;
}

bool cmpAC(shared_ptr<CPerson> lhs, shared_ptr<CPerson> rhs)
{
  if (lhs->getAccount().compare(rhs->getAccount()) < 0)
  {
    return true;
  }
  return false;
}

bool CIterator::AtEnd(void) const
{
  return dbIterator == dbEnd;
}

void CIterator::Next(void)
{
  dbIterator ++;
}

string CIterator::Name(void) const
{
  return dbIterator -> get() -> getName();
}

string CIterator::Addr(void) const
{
  return dbIterator -> get() -> getAddress();
}

string CIterator::Account(void) const
{
  return dbIterator -> get() -> getAccount();
}

void CIterator::setIterator(vector<shared_ptr<CPerson>>::const_iterator begin, vector<shared_ptr<CPerson>>::const_iterator end)
{
  dbIterator = begin;
  dbEnd = end;
}

CPerson::CPerson(const string &name, const string &addr, const string &account)
{
  this->name = name;
  address = addr;
  this->account = account;
  expense = 0;
  income = 0;
}

void CPerson::addIncome(int amount)
{
  this -> income += amount;
}

void CPerson::addExpense(int amount)
{
  this -> expense += amount;
}

void CPerson::setAccount(const string &account)
{
  this -> account = account;
}

bool CTaxRegister::Birth(const string &name, const string &addr, const string &account)
{
  shared_ptr<CPerson> temp = make_shared<CPerson>(name, addr, account);
  auto iterD = database.begin();
  auto iterI = index.begin();
  //checking if name and address are unique
  //checking if account is unique
  if (binary_search(database.begin(), database.end(), temp, cmpNA) == false && binary_search(index.begin(), index.end(), temp, cmpAC) == false )
  {
    //if person is unique add person to Database;
    iterD = upper_bound(database.begin(), database.end(), temp, cmpNA);
    iterI = upper_bound(index.begin(), index.end(), temp, cmpAC);
    database.insert(iterD, temp);
    index.insert(iterI, temp);
    return true;
  }

  return false;
}

bool CTaxRegister::Income(const string &account, int amount)
{
  //creating temporary person for search reasons
  shared_ptr<CPerson> temp = make_shared<CPerson>(" ", " ", account);
  auto iter = lower_bound(index.begin(), index.end(), temp, cmpAC);
  //testing if person exists
  if(iter == index.end() || iter -> get() -> getAccount() != account)
  {
    return false;
  }
  else
  {
    //if exists add to their income
    iter -> get() -> addIncome(amount);
  }
  return true;
}

bool CTaxRegister::Income(const string &name, const string &addr, int amount)
{
  
  shared_ptr<CPerson> temp = make_shared<CPerson>(name, addr, " ");
  auto iter = lower_bound(database.begin(), database.end(), temp, cmpNA);
  //testing if person exists
  if(iter == database.end() || iter -> get() -> getName() != name || iter -> get() -> getAddress() != addr )
  {
    return false;
  }
  else
  {
    iter -> get() -> addIncome(amount);
  }
  return true;
}

bool CTaxRegister::Expense(const string &account, int amount)
{
  //creating temporary person for search reasons
  shared_ptr<CPerson> temp = make_shared<CPerson>(" ", " ", account);
  auto iter = lower_bound(index.begin(), index.end(), temp, cmpAC);
  //testing if person exists
  if(iter == index.end() || iter -> get() -> getAccount() != account)
  {
    return false;
  }
  else
  {
    //if exists add to their expense
    iter -> get() -> addExpense(amount);
  }
  return true;
}
  
bool CTaxRegister::Expense(const string &name, const string &addr, int amount)
{
  shared_ptr<CPerson> temp = make_shared<CPerson>(name, addr, " ");
  auto iter = lower_bound(database.begin(), database.end(), temp, cmpNA);
  //testing if person exists
  if(iter == database.end() || iter -> get() -> getName() != name || iter -> get() -> getAddress() != addr)
  {
    return false;
  }
  else
  {
    iter -> get() -> addExpense(amount);
  }
  return true;
}

bool CTaxRegister::Audit(const string &name, const string &addr, string &account, int &sumIncome, int &sumExpense) const
{
  shared_ptr<CPerson> temp = make_shared<CPerson>(name, addr, " ");
  auto iter = lower_bound(database.begin(), database.end(), temp, cmpNA);
  //testing if person exists
  if(iter == database.end() || iter -> get() -> getName() != name || iter -> get() -> getAddress() != addr)
  {
    return false;
  }
  else
  {
    account = iter -> get() -> getAccount();
    sumIncome = iter -> get() -> getIncome();
    sumExpense = iter -> get() -> getExpense();
  }
  return true;
}

CIterator CTaxRegister::ListByName(void) const
{
  CIterator tmp;
  tmp.setIterator(database.begin(), database.end());
  return tmp;
}

bool CTaxRegister::Death(const string &name, const string &addr)
{
  shared_ptr<CPerson> temp = make_shared<CPerson>(name, addr, " ");
  auto iterD = database.begin();
  auto iterI = index.begin();

  if(binary_search(database.begin(), database.end(), temp, cmpNA) == false)
  {
    return false;
  }
  else
  {
    iterD = lower_bound(database.begin(), database.end(), temp, cmpNA);
    temp -> setAccount(iterD -> get() -> getAccount());
    iterI = lower_bound(index.begin(), index.end(), temp, cmpAC);
    database.erase(iterD);
    index.erase(iterI); 
  }
  


  return true;
}

#ifndef __PROGTEST__
int main(void)
{
  string acct;
  int sumIncome, sumExpense;
  CTaxRegister b1;
  assert(b1.Birth("John Smith", "Oak Road 23", "123/456/789"));
  assert(b1.Birth("Jane Hacker", "Main Street 17", "Xuj5#94"));
  assert(b1.Birth("Peter Hacker", "Main Street 17", "634oddT"));
  assert(b1.Birth("John Smith", "Main Street 17", "Z343rwZ"));
  //Testing birth of non unique people
  assert(!b1.Birth("John Smith", "Oak Road 23", "Z343rwZ"));
  assert(!b1.Birth("John Doe", "Main Street 9.75", "Z343rwZ"));

  assert(b1.Income("Xuj5#94", 1000));
  assert(b1.Income("634oddT", 2000));
  assert(b1.Income("123/456/789", 3000));
  assert(b1.Income("634oddT", 4000));
  assert(b1.Income("Peter Hacker", "Main Street 17", 2000));
  //Testing adding income for nonexistent people

  assert(!b1.Income("0912TRe", 1000));
  assert(!b1.Income("John Doe", "Main Street 9.75", 2000));

  assert(b1.Expense("Jane Hacker", "Main Street 17", 2000));
  assert(b1.Expense("John Smith", "Main Street 17", 500));
  assert(b1.Expense("Jane Hacker", "Main Street 17", 1000));
  assert(b1.Expense("Xuj5#94", 1300));
  //Testing adding income for nonexistent people

  assert(!b1.Expense("0912TRe", 1000));
  assert(!b1.Expense("John Doe", "Main Street 9.75", 2000));

  assert(b1.Audit("John Smith", "Oak Road 23", acct, sumIncome, sumExpense));
  assert(acct == "123/456/789");
  assert(sumIncome == 3000);
  assert(sumExpense == 0);
  assert(b1.Audit("Jane Hacker", "Main Street 17", acct, sumIncome, sumExpense));
  assert(acct == "Xuj5#94");
  assert(sumIncome == 1000);
  assert(sumExpense == 4300);
  assert(b1.Audit("Peter Hacker", "Main Street 17", acct, sumIncome, sumExpense));
  assert(acct == "634oddT");
  assert(sumIncome == 8000);
  assert(sumExpense == 0);
  assert(b1.Audit("John Smith", "Main Street 17", acct, sumIncome, sumExpense));
  assert(acct == "Z343rwZ");
  assert(sumIncome == 0);
  assert(sumExpense == 500);
  CIterator it = b1.ListByName();
  assert(!it.AtEnd() && it.Name() == "Jane Hacker" && it.Addr() == "Main Street 17" && it.Account() == "Xuj5#94");
  it.Next();
  assert(!it.AtEnd() && it.Name() == "John Smith" && it.Addr() == "Main Street 17" && it.Account() == "Z343rwZ");
  it.Next();
  assert(!it.AtEnd() && it.Name() == "John Smith" && it.Addr() == "Oak Road 23" && it.Account() == "123/456/789");
  it.Next();
  assert(!it.AtEnd() && it.Name() == "Peter Hacker" && it.Addr() == "Main Street 17" && it.Account() == "634oddT");
  it.Next();
  assert(it.AtEnd());

  assert(b1.Death("John Smith", "Main Street 17"));

  CTaxRegister b2;
  assert(b2.Birth("John Smith", "Oak Road 23", "123/456/789"));
  assert(b2.Birth("Jane Hacker", "Main Street 17", "Xuj5#94"));
  assert(!b2.Income("634oddT", 4000));
  assert(!b2.Expense("John Smith", "Main Street 18", 500));
  assert(!b2.Audit("John Nowak", "Second Street 23", acct, sumIncome, sumExpense));
  assert(!b2.Death("Peter Nowak", "5-th Avenue"));
  assert(!b2.Birth("Jane Hacker", "Main Street 17", "4et689A"));
  assert(!b2.Birth("Joe Hacker", "Elm Street 23", "Xuj5#94"));
  assert(b2.Death("Jane Hacker", "Main Street 17"));
  assert(b2.Birth("Joe Hacker", "Elm Street 23", "Xuj5#94"));
  assert(b2.Audit("Joe Hacker", "Elm Street 23", acct, sumIncome, sumExpense));
  assert(acct == "Xuj5#94");
  assert(sumIncome == 0);
  assert(sumExpense == 0);
  assert(!b2.Birth("Joe Hacker", "Elm Street 23", "AAj5#94"));


  //extra tests to check extra behavior.
  CTaxRegister b3;
  assert(!b3.Death("test", "trt 215"));
  CIterator iter = b3.ListByName();
  assert(iter.AtEnd());
  assert(!b3.Income("test", "trt 215", 200));
  assert(!b3.Expense("test", "trt 215", 200));
  assert(!b2.Income("634oddT", 4000));
  assert(!b2.Expense("634oddT", 4000));
  assert(!b3.Audit("test", "trt 215", acct, sumIncome, sumExpense));
  return 0;
}
#endif /* __PROGTEST__ */
