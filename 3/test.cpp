#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cstdint>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#include <functional>
#include <stdexcept>
using namespace std;
#endif /* __PROGTEST__ */

class CBigInt
{
  public:
    CBigInt();
    CBigInt(const int);
    CBigInt(const char *);
    // copying/assignment/destruction
    friend  CBigInt operator + (const CBigInt &lhs, const CBigInt &rhs);
    friend  CBigInt operator - (const CBigInt &lhs, const CBigInt &rhs);
    friend  CBigInt operator * (const CBigInt &lhs, const CBigInt &rhs);
    CBigInt & operator += (const CBigInt &);
    CBigInt & operator -= (const CBigInt &);
    CBigInt & operator *= (const CBigInt &);
    // comparison operators, any combination {CBigInt/int/string} {<,<=,>,>=,==,!=} {CBigInt/int/string}
    friend bool operator == (const CBigInt &lhs, const CBigInt &rhs);
    friend bool operator != (const CBigInt &lhs, const CBigInt &rhs);
    friend bool operator < (const CBigInt &lhs, const CBigInt &rhs);
    friend bool operator <= (const CBigInt &lhs, const CBigInt &rhs);
    friend bool operator > (const CBigInt &lhs, const CBigInt &rhs);
    friend bool operator >= (const CBigInt &lhs, const CBigInt &rhs);
    friend ostream & operator << (ostream & out, const CBigInt & ); 
    friend istream & operator >> (istream & iss, CBigInt & );
    friend void testSplit();
    friend void testmultiplyByDigit();
    friend void testKaratsuba();
    friend void testSum();
  private:
    u_int16_t sum (const u_int16_t &a, const u_int16_t &b, u_int16_t &carry);
    u_int16_t diff (const u_int16_t &a, const u_int16_t &b, u_int16_t &carry);
    CBigInt  karatsuba(const CBigInt &lhs, const CBigInt &rhs);
    CBigInt  mul(const CBigInt &lhs, const CBigInt &rhs);
    CBigInt  multiplyByDigit(const u_int16_t, const CBigInt &rhs);
    void split(const CBigInt &toSplit, CBigInt &low, CBigInt &high, unsigned int num);
    //shifts this CBigInt by num chunks
    CBigInt shift(int num);
    void cleanUp();
    bool sign;
    //from lowest to highest
    vector<u_int16_t> number;
};
//from int to bin mask and shift
//from bin to int add and shift

static bool equal ( const CBigInt & x, const char * val )
{
  ostringstream oss;
  oss << x;
  return oss . str () == val;
}

CBigInt::CBigInt()
{
  sign = false;
  number.push_back(0);
}

CBigInt::CBigInt(const int num)
{
  u_int16_t top = 0, bottom = 0;
  if(num < 0)
  {
    sign = true;
    top = (abs(num) >> 16) ;
    bottom = abs(num) & 0xFFFF;
  }
  else
  {
    sign = false;
    top = num >> 16 ;
    bottom = num & 0xFFFF;
  }
  

  // u_int16_t top = (abs(num) >> 16) ;
  // u_int16_t bottom = abs(num) & 0xFFFF;

  number.push_back(bottom);
  if(top != 0)
  {
    number.push_back(top);
  }
}
CBigInt CBigInt::shift(int num)
{
  for(int i = 0; i < num; i++)
  {
    number.insert(number.begin(), 0);
  }
  return *this;
}
CBigInt::CBigInt(const char * num)
{
  int begin = 0;
  bool sig = false;
  sign = false;
  if(num[0] == '-')
  {
    sig = true;
    begin = 1;
  }
  for(int i = begin, index = 0; i < strlen(num); i++, index ++)
  {
    if( num[i] < '0' || num[i] > '9')
    {
      throw invalid_argument("Not a number");
    }
    if(index != 0)
    {
      *this *= 10;
    }
    *this += (num[i] - '0');
  }
  sign = sig;
  //cout << *this << endl;
}

u_int16_t CBigInt::sum (const u_int16_t &a, const u_int16_t &b, u_int16_t &carry)
{
  u_int32_t result = 0;

  result = a + b + carry;

  carry = (result >> 16);

  return result & 0xFFFF;
}

void CBigInt::split(const CBigInt &toSplit, CBigInt &low, CBigInt &high, unsigned int num)
{
  low.number = vector<u_int16_t> (toSplit.number.begin(), toSplit.number.begin() + num);
  high.number = vector<u_int16_t> (toSplit.number.begin() + num, toSplit.number.end());
}

void testSplit()
{
  CBigInt low, high, test;
  int size = 0;
  //split two chunk;
  test = 1000000;
  size = test.number.size();
  size = floor(size/2);
  test.split(test, low, high, size);
  assert(equal(low, "16960"));
  assert(equal(high, "15"));
  //split three chunks
  test = "10000000000";
  size = test.number.size();
  size = floor(size/2);
  test.split(test, low, high, size);
  assert(equal(low, "58368"));
  assert(equal(high, "152587"));
  test = "281474976710656";
  size = test.number.size();
  size = floor(size/2);
  test.split(test, low, high, size);
  assert(equal(low, "00"));
  assert(equal(high, "65536"));
  //split four chunks
  test = "1000000000000000";
  size = test.number.size();
  size = floor(size/2);
  test.split(test, low, high, size);
  assert(equal(low, "2764472320"));
  assert(equal(high, "232830"));
}

CBigInt  CBigInt::multiplyByDigit(const u_int16_t num, const CBigInt &rhs)
{
  CBigInt result;
  int i = 0;
  for(auto iter = rhs.number.begin(); iter != rhs.number.end(); iter++, i++)
  {
    CBigInt tmp = (*iter)*num;
    tmp.shift(i);
    result += tmp;
  }
  return result;
}

void testmultiplyByDigit()
{
  u_int16_t a = 0;
  CBigInt rhs;
  CBigInt res;
  //one chunk
  a = 5;
  res = rhs.multiplyByDigit(a, rhs);
  assert(equal(res, "0"));

  rhs = 10;
  res = rhs.multiplyByDigit(a, rhs);
  assert(equal(res, "50"));

  a = 1;
  res = rhs.multiplyByDigit(a, res);
  assert(equal(res, "50")); 
  //two chunks
  a = 500;
  rhs = 1000000;
  res = rhs.multiplyByDigit(a, rhs);
  assert(equal(res, "500000000"));

  a = 4;
  rhs = 65536;
  res = rhs.multiplyByDigit(a, rhs);
  cout << res << endl;
  assert(equal(res, "262144"));
  //three chunks
  a = 500;
  rhs = "10000000000";
  res = rhs.multiplyByDigit(a, rhs);
  assert(equal(res, "5000000000000"));
  
  //misc tests
  a = 0;
  rhs = 0;
  res = rhs.multiplyByDigit(a, rhs);
  assert(equal(res, "0"));
  a = 1;
  rhs = 32152;
  res = rhs.multiplyByDigit(a, rhs);
  assert(equal(res, "32152"));
  a = 4;
  res = rhs.multiplyByDigit(a, rhs);
  assert(equal(res, "128608"));

  rhs = "2147483648";
  res = rhs.multiplyByDigit(a, rhs);
  cout << res << endl;
  assert(equal(res, "8589934592"));

}
u_int16_t CBigInt::diff(const u_int16_t &a, const u_int16_t &b, u_int16_t &carry)
{
  u_int32_t result = 0;
  if(a >= b - carry)
  {
    result = a - b - carry;
    carry = 0;
  }
  else
  {
    result = a + 65536 - b - carry;
    carry = 1;
  }

  return result & 0xFFFF;
}
CBigInt  CBigInt::karatsuba(const CBigInt &lhs, const CBigInt &rhs)
{
  if(lhs.number.size() == 1 || rhs.number.size() == 1)
  {
    if(lhs.number.size() == 1)
    {
      return multiplyByDigit(lhs.number.at(0),rhs);
    }
    else
    {
      return multiplyByDigit(rhs.number.at(0),lhs);
    }
    
  }

  int size = min(lhs.number.size(), rhs.number.size());
  int split = ceil(size/2);

  CBigInt high1, low1, high2, low2;  
  
  this -> split(lhs, low1, high1, split);
  this -> split(rhs, low2, high2, split);

  CBigInt z0 = karatsuba(low1, low2);
  CBigInt z1 = karatsuba(low1 + high1, low2 + high2);
  CBigInt z2 = karatsuba(high1, high2);

  return z2.shift(2*split) + (z1 - z2 - z0).shift(split) +  z0; 
}

void testKaratsuba()
{
  CBigInt a, b, res;
  a = 65536;
  b = 65536;
  res = res.karatsuba(a, b);
  assert(equal(res, "4294967296"));
}

CBigInt  CBigInt::mul(const CBigInt &lhs, const CBigInt &rhs)
{
  CBigInt result;
  unsigned int iA = 0;
  for(auto a = lhs.number.begin(); a < lhs.number.end(); a++, iA ++)
  {
    unsigned int iB = 0;
    for(auto b = rhs.number.begin(); b < rhs.number.end(); b++, iB++)
    {
      CBigInt tmp = (*a)*(*b);
      tmp.sign = false;
      for(unsigned int i = 0; i < iA + iB; i++)
      {
        tmp.number.insert(tmp.number.begin(), 0);
      }
      result += tmp;
    }
  }
  return result;
}

void CBigInt::cleanUp()
{
  if(*(number.end() - 1) == 0)
  {
    for(auto it = number.end() -1; it > number.begin(); it --)
    {
      if(*it == 0)
      {
        number.pop_back();
      }
      else
      {
        break;
      }
    }
  }
  if((*(number.end() - 1) == 0))
  {
    sign = false;
  }
}

CBigInt operator + (const CBigInt &lhs, const CBigInt &rhs)
{
  CBigInt result = lhs;
  return result += rhs;
}

CBigInt operator - (const CBigInt &lhs, const CBigInt &rhs)
{
  CBigInt result = lhs;
  return result -= rhs;
}

CBigInt operator * (const CBigInt &lhs, const CBigInt &rhs)
{
  CBigInt result = lhs;
  return result *= rhs;
}

CBigInt & CBigInt::operator -= (const CBigInt & b)
{
  unsigned int length = max(number.size(), b.number.size());
  u_int16_t carry = 0;
  bool sig = sign;

  CBigInt lhs, rhs;
  lhs = *this;
  lhs.sign = false;
  rhs = b;
  rhs.sign = false;

  if((lhs < rhs))
  {
    CBigInt tmp = lhs;
    lhs = rhs;
    rhs = tmp;
    sig = !sig;
  }
  
  for(unsigned int i = 0; i <length; i++)
  {
    if(i >= rhs.number.size())
    {
      if(i >= number.size())
      {
        number.push_back(diff(lhs.number.at(i), 0, carry));
      }
      else
      {
        number.at(i) = diff(lhs.number.at(i), 0, carry);
      }
    }
    else
    {
      number.at(i) = diff(lhs.number.at(i), rhs.number.at(i), carry);
    }
  }

  sign = sig;
  cleanUp();
  return *this;
}


CBigInt & CBigInt::operator += (const CBigInt & b)
{
  unsigned int length = max(number.size(), b.number.size());
  u_int16_t carry = 0;

  for(unsigned int i = 0; i < length; i++)
  {
    if(i >= number.size())
    {
      number.push_back(sum(b.number.at(i), 0,  carry));
    }
    else
    {
      if(i >= b.number.size())
      {
        length = i;
        break;
      }
      if(sign == b.sign)
      {
        number.at(i) = sum(number.at(i), b.number.at(i), carry);
      }
      else
      {
        *this -= b;
        return *this;
      } 
    }
  }
  while(carry != 0)
  {
    if(length >= number.size())
    {
      number.push_back(carry);
      carry = 0;
    }
    number.at(length) = sum(number.at(length), 0,  carry);
    length ++;
  }
  cleanUp();
  return *this;
}

void testSum()
{
  CBigInt a,b;
  //testing carry over into new chunks...
  a = 1;
  b = 65535;
  b+=a;
  assert(equal(b, "65536"));

  a = 2147483647;
  b = 1;
  a += b;
  cout << a << endl;
  assert(equal(a, "2147483648"));

  a = "281474976710655";
  b = 1;
  a += b;
  cout << a << endl;
  assert(equal(a, "281474976710656"));

}
CBigInt & CBigInt::operator *= (const CBigInt &b)
{
  bool sig = true;
  //set the sign
  if(sign != b.sign)
  {
    sig = true;
  }
  else
  {
    sig = false;
  }
  *this = karatsuba(*this, b);
  sign = sig;
  cleanUp();
  return *this;
}

bool operator == (const CBigInt &lhs, const CBigInt &rhs)
{
  if(lhs.sign != rhs.sign)
  {
    return false;
  }
  if(lhs.number.size() != rhs.number.size())
  {
    return false;
  }
  auto it = lhs.number.begin();
  for(auto iter = rhs.number.begin(); iter < rhs.number.end(); iter ++, it++)
  {
    if(*iter != *it)
    {
      return false;
    }
  }
  return true;
}
bool operator != (const CBigInt &lhs, const CBigInt &rhs)
{
  return !(lhs == rhs);
}
bool operator < (const CBigInt &lhs, const CBigInt &rhs)
{
  //if left is positive and right is negative return false
  if(!lhs.sign && rhs.sign)
  {
    return false;
  }
  //if left is longer than right return false
  if(lhs.number.size() > rhs.number.size())
  {
    return false;
  }
  if(lhs.number.size() < rhs.number.size())
  {
    return true;
  }
  if(lhs == rhs)
  {
    return false;
  }
  auto it = lhs.number.end() - 1;
  //check the numbers from biggest to smallest and if lhs chunk is bigger than rhs chunk return false
  for(auto iter = rhs.number.end() - 1; iter >= rhs.number.begin(); iter--, it--)
  {
    if(*it > *iter)
    {
      return false;
    }
  }

  return true;
  //return (lhs - rhs).sign == true;
}
bool operator <= (const CBigInt &lhs, const CBigInt &rhs)
{
  return !(lhs > rhs);
}
bool operator > (const CBigInt &lhs, const CBigInt &rhs)
{
  //if right is positive and left is negative return false
  if(lhs.sign && !rhs.sign)
  {
    return false;
  }
  //if right is longer than left return false
  if(lhs.number.size() < rhs.number.size())
  {
    return false;
  }
  if(lhs.number.size() > rhs.number.size())
  {
    return true;
  }
  if(lhs == rhs)
  {
    return false;
  }
  auto it = lhs.number.end() - 1;
  //check the numbers from biggest to smallest and if lhs chunk is bigger than rhs chunk return false
  for(auto iter = rhs.number.end() - 1; iter >= rhs.number.begin(); iter--, it--)
  {
    if(*it < *iter)
    {
      return false;
    }
  }

  return true;
  //return (lhs - rhs).sign == false;
}
bool operator >= (const CBigInt &lhs, const CBigInt &rhs)
{
  return !(lhs < rhs);
}

ostream & operator << (ostream & out, const CBigInt &o)
{
  u_int32_t chunk = 0;
  u_int16_t remainder = 0;
  string output;
  CBigInt tmp = o;
  
  if(tmp.sign == true && *(tmp.number.end() - 1) != 0)
  {
    out << "-";
  }

  while(tmp.number.size() > 0)
  {
    for(auto it = tmp.number.end() -1; it >= tmp.number.begin(); it --)
    {
      // if(*it == 0)
      // {
      //   continue;
      // }
      if(it == tmp.number.end() -1)
      {
        chunk = *it;
      }
      else
      {
        chunk = (remainder << 16) + *it;
      }
      remainder = chunk % 10;
      *it = chunk / 10;
    }
    if(tmp.number.size() != 0)
    {
      output.insert(0, to_string(remainder));
    }
    if(*(tmp.number.end() - 1) == 0)
    {
      tmp.number.erase((tmp.number.end() - 1));
    }
  }
  out << output;
  return out;
}

istream & operator >> (istream & iss, CBigInt & in)
{
  CBigInt tmp = in;
  in = 0;
  char digit = '0';
  int index = 0;

  iss >> ws;

  if(iss.peek() == '-')
  {
    in.sign = true;
    //clear ths minus
    iss >> digit;
    //load first digit
  }
  
  while((digit = iss.peek()))
  {
    if(digit > '9' || digit < '0')
    {
      if(index == 0)
      {
        in = tmp;
        iss . setstate (ios::failbit);
      }
      break;
    }
    else
    {
      iss >> digit;
      if(index != 0)
      {
        in *= 10;
      }
      in += (digit - '0');
    }
    index++; 
  }

  return iss;
}

#ifndef __PROGTEST__

int main ( void )
{
  CBigInt a, b, c, d, e, f, g, h, i, j, k;
  istringstream is;

  testSum();
  testmultiplyByDigit();
  testKaratsuba();
  testSplit();

  //multiplication by zero
  i = 0;
  i*= 5;
  assert(equal(i, "0"));
  j*=0;
  assert(equal(j, "0"));
  //multiplication by one
  j+= 10000000;
  j*=1;
  assert(equal(j, "10000000"));
  //two chunks;
  k = 100000;
  j = k * 100000;
  //cout << j << endl;
  assert(equal(j, "10000000000"));

  //subtraction
  i+= -100000;
  assert(equal(i, "-100000"));
  i += 1;

  i = 0;

  i+= 1;
  assert(equal(i, "1"));
  i += -100000;
  //cout << i << endl;
  assert(equal(i, "-99999"));
  j = 0;
  j += -5;
  //cout << j << endl;
  assert(equal(j, "-5"));
  j += 5;
  assert(equal(j, "0"));

  i = "50";
  k = 0;
  k = "5"*k;
  k+= 5;
  k *= "5";
  assert(equal(k, "25"));
  k*= 10;
  assert(equal(k, "250"));
  k= k - "250";
  k*="5";
  k*=5;
  assert(equal(k, "0"));

  k = (5 * 50);
  assert(equal(k, "250"));
  k = 0;
  k+=20;
  k*= "20";
  assert(equal(k, "400"));

   k = k - "400";
  assert(equal(k, "0"));

  // e += "123567890987";
  // assert(equal(e, "123567890987"));
  // e *= "7890987654321";
  // assert(equal(e, "975072702248900167504827"));
  // f = "9876543210";
  // f*= "21";
  // assert(equal(f, "207407407410"));
  // e*=f;
  // k =  "20223730120970726026609089673056807";
  // assert(equal(e, "20223730120970726026609089673056807"));
  // f = e;
  // e*= f;
  // cout << e << endl;
  // assert(equal(e, "999325989001669472783324942817116082738663801"));
  // c = "1234567890";
  // assert(equal(c, "1234567890"));
  // d = "-2345678901";
  // cout << c << " += " << d << " = " << (c+=d) << endl;
  // assert(equal(c, "-1111111011"));

  a = 10;
  a += 20;
  assert ( equal ( a, "30" ) );
  a *= 5;
  assert ( equal ( a, "150" ) );
  b = a + 3;
  assert ( equal ( b, "153" ) );
  b = a * 7;
  assert ( equal ( b, "1050" ) );
  assert ( equal ( a, "150" ) );

  a = 10;
  a += -20;
  assert ( equal ( a, "-10" ) );
  a *= 5;
  assert ( equal ( a, "-50" ) );
  b = a + 73;
  assert ( equal ( b, "23" ) );
  b = a * -7;
  assert ( equal ( b, "350" ) );
  assert ( equal ( a, "-50" ) );

  a = "12345678901234567890";
  a += "-99999999999999999999";
  assert ( equal ( a, "-87654321098765432109" ) );
  //THIS TEST DOESNT WORK
  a *= "54321987654321987654";
  cout << a << endl;
  //Result: -4421362849512323703622067671434455935566
  assert ( equal ( a, "-4761556948575111126880627366067073182286" ) );

  a *= 0;
  assert ( equal ( a, "0" ) );

  a = 10;
  b = a + "400";
  assert ( equal ( b, "410" ) );
  b = a * "15";
  assert ( equal ( b, "150" ) );
  assert ( equal ( a, "10" ) );

  is . clear ();
  is . str ( " 1234" );
  //is >> b;
  assert ( is >> b );
  assert ( equal ( b, "1234" ) );
  is . clear ();
  is . str ( " 12 34" );
  assert ( is >> b );
  cout << b << endl;
  assert ( equal ( b, "12" ) );
  is . clear ();
  is . str ( "999z" );
  assert ( is >> b );
  assert ( equal ( b, "999" ) );
  is . clear ();
  is . str ( "abcd" );
  assert ( ! ( is >> b ) );
  is . clear ();
  is . str ( "- 758" );
  assert ( ! ( is >> b ) );
  a = 42;
  try
  {
    a = "-xyz";
    assert ( "missing an exception" == NULL );
  }
  catch ( const invalid_argument & e )
  {
    assert ( equal ( a, "42" ) );
  }

  a = "73786976294838206464";
  assert ( a < "1361129467683753853853498429727072845824" );
  assert ( a <= "1361129467683753853853498429727072845824" );
  assert ( ! ( a > "1361129467683753853853498429727072845824" ) );
  assert ( ! ( a >= "1361129467683753853853498429727072845824" ) );
  assert ( ! ( a == "1361129467683753853853498429727072845824" ) );
  assert ( a != "1361129467683753853853498429727072845824" );
  assert ( ! ( a < "73786976294838206464" ) );
  assert ( a <= "73786976294838206464" );
  assert ( ! ( a > "73786976294838206464" ) );
  assert ( a >= "73786976294838206464" );
  assert ( a == "73786976294838206464" );
  assert ( ! ( a != "73786976294838206464" ) );
  assert ( a < "73786976294838206465" );
  assert ( a <= "73786976294838206465" );
  assert ( ! ( a > "73786976294838206465" ) );
  assert ( ! ( a >= "73786976294838206465" ) );
  assert ( ! ( a == "73786976294838206465" ) );
  assert ( a != "73786976294838206465" );
  a = "2147483648";
  assert ( ! ( a < -2147483648 ) );
  assert ( ! ( a <= -2147483648 ) );
  assert ( a > -2147483648 );
  assert ( a >= -2147483648 );
  assert ( ! ( a == -2147483648 ) );
  assert ( a != -2147483648 );
  return 0;
}
#endif /* __PROGTEST__ */
